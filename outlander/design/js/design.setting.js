$(function() {

	var userAgent = window.navigator.userAgent.toLowerCase();
	var appVersion = window.navigator.appVersion.toLowerCase();
	var isIE8 = false;
	if(userAgent.indexOf("msie") != -1){
		if(appVersion.indexOf("msie 8.") != -1) {
			isIE8 = true;
		}
	}

	$('.bxslider01').bxSlider({
		pager:true,
		pagerCustom: '#bx-pager01',
		speed: 200,
		controls: false,
		mode: 'fade',
		onSlideAfter: function(obj, b, current) {
			/*if (isIE8) {
				var offset = $('#Mainvis').offset().top;
				$('body, html').animate({scrollTop : offset}, 500);
			}*/
		}
	});


	
	$('#flexslider').flexslider({
		animation: "slide",
		controlNav: true,
		animationLoop: false,
		slideshow: false,
		sync: "#carousel,#carousel2",
		directionNav: true,
		prevText: '<img src="./images/btn_prev_flex.jpg">',
		nextText: '<img src="./images/btn_next_flex.jpg">'
	});

	$('#carousel').flexslider({
		animation: "slide",
		controlNav: false,
		animationLoop: false,
		slideshow: false,
		itemWidth: 174,
		asNavFor: '#flexslider',
		prevText: '<img src="./images/carousel_prev.png">',
		nextText: '<img src="./images/carousel_next.png">'
	});

});
